# reconnecting-map

## Name

reconnecting-map

## Description

A tool to make a general mapping of the Reconnecting Objects research projects. See https://reconnecting.art for more info about Reconnecting Objects. This is an early implementation of *remap*, a tool for research. See https://gitlab.com/lavillahermosa/remap for the remap repo.