
const Session = ({
    getControl = null,
    onControlChange = null
} = {}) => {
    let status;

    const takeControl = async() => {
        const response = await getControl?.();
        return response;
    }

    const controlLoop = async() => {
        const newStatus = await takeControl();
        console.log(newStatus);
        if(newStatus != status)
            onControlChange?.(newStatus);
        status = newStatus;
        setTimeout(controlLoop, 60000);

    }

    const hasControl = () => {
        return status['control']?status['control']:false;
    }

    return {controlLoop, hasControl};
};

export default Session;