import Router from './js/Router.js';
import Interface from './js/Interface.js';
import Session from './js/Session.js';

const router =  Router();

const itf = Interface({
    getNodeTypes:router.getNodeTypes,
    getEdgeTypes:router.getEdgeTypes,
    getNodes:router.getNodes,
    saveNode:router.saveNode,
    updateNode:router.updatePage,
    getEdges:router.getEdges,
    saveEdge:router.saveEdge,
    updateEdge:router.updatePage,
    onRemoveEdge:router.removeEdge,
    onUpEdge:router.upEdge
});

const session = Session({
    getControl:router.getControl,
    onControlChange:itf.setControl
});

session.controlLoop();

window.addEventListener('load', e=>{
    document.querySelectorAll('.loader-font-1').forEach(($loader) => {
        $loader.remove();
    });
    itf.init();
});