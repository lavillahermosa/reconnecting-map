<?php namespace ProcessWire;

if(!defined("PROCESSWIRE")) die();

/** @var ProcessWire $wire */

/**
 * ProcessWire Bootstrap API Ready
 * ===============================
 * This ready.php file is called during ProcessWire bootstrap initialization process.
 * This occurs after the current page has been determined and the API is fully ready
 * to use, but before the current page has started rendering. This file receives a
 * copy of all ProcessWire API variables.
 *
 */

 $wire->addHookAfter('InputfieldPage::getSelectablePages', function($event) {
    if($event->object->hasField == 'node_a' || $event->object->hasField == 'node_b') {
      $parent = $event->pages->get('/nodes/');
      $event->return = $event->pages->find('parent='.$parent->children);
    }
  });